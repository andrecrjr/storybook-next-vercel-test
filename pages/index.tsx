import Button from "../components/Button/Button";
import ThemeProvider from "../components/theme/ThemeProvider";

export default function Home(){
    
    return <ThemeProvider><Button>Entrei aquii</Button></ThemeProvider>
}