const theme= {
    colors:{
        primary:{
            main:"#2176a6",
            dark:"#185784",
            light:"#2d99cd",
            text: "#b5e4f3"
        },
        accent:{
            main:"#606060",
            dark:"#3c3c3c",
            light:"#979797",
            text: "#fafafa"
        }
    }
}

export default theme;