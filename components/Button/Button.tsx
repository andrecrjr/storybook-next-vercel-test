import React from "react";
import styled from "styled-components";

interface ButtonProps {
  /** Essa prop é baseada nas opções do nosso tema  */
  disabled?:boolean;
  /** Essa prop é baseada nas opções do nosso tema  */
  variant?:'primary'|'accent'
  children: React.ReactNode
  onClick?: React.MouseEventHandler<HTMLButtonElement>
}

export const StyledButton = styled.button<ButtonProps>`
cursor:pointer;
border:0;
padding: 8px 8px;
transition: 0.4;
${({theme, variant})=>{
  console.log(theme)
  return {
    backgroundColor: theme.colors[variant]?.main,
    color: theme.colors[variant]?.text
  }
}}

`

function Button({children, ...props}:ButtonProps) {
  return <StyledButton {...props}>{children}</StyledButton>;
}

Button.defaultProps = {
  disabled:false,
  variant:"primary"
}

export default Button;

