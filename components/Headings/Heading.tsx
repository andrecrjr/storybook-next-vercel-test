import React from "react";


interface HeadingProps {
    children: React.ReactNode,
    as: "h1"|"h2"|"h3"|"h4"|"h5"|"h6"
}

export default function Heading({children, as, ...props}:HeadingProps) {
  const newElement = React.createElement(as, props, children)
  return newElement
}

Heading.defaultProps = {
  as:"h1"
}

