import {create} from "@storybook/theming";


export default create({
    base:'dark',
    brandTitle:"Eroshi inc.",
    "brandTarget":"_self"
})