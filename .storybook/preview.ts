import type { Preview, Decorator } from "@storybook/react";
import {withThemeStorybook} from '../components/theme/ThemeProvider'


export const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
};



export const decorators = [
  withThemeStorybook
]

